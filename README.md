## Podcast sentiment analysis

Dataset: https://www.kaggle.com/datasets/thoughtvector/podcastreviews

Streamlit: https://podcast-sentiment-analysis.streamlit.app/

- `podcast-sentiment-analysis.ipynb` - preprocesses data and creates RNN to predict positive/negative sentiment
- `predict-podcast-review` - uses the saved RNN model to make a prediction
- `api/app.py` - API endpoint that takes a review as input and makes a prediction
- `streamlit.py` - deploy app using streamlit
