from flask import Flask, request
import pickle
import tensorflow as tf
import numpy as np

app = Flask(__name__)

podcast_review_model = pickle.load(open('../podcast_review_model.pkl', 'rb'))
tokenizer = pickle.load(open('../tokenizer.pkl', 'rb'))

SEQ_LEN = 128
def prepare_texts(texts: list) -> np.array:
    return tf.keras.preprocessing.sequence.pad_sequences(tokenizer.texts_to_sequences(texts), maxlen=SEQ_LEN)
 
@app.route('/predict', methods=['POST'])
def predict():
    json = request.get_json()
    prediction = podcast_review_model.predict(prepare_texts([json['review']])).argmax(axis=1)

    if (prediction[0] == 0):
        return "Negative"

    return "Positive"
 
# main driver function
if __name__ == '__main__':
    app.run()