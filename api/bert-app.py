from flask import Flask, request
from transformers import pipeline

app = Flask(__name__)
 
@app.route('/predict', methods=['POST'])
def predict():
    json = request.get_json()

    sentiment_pipeline = pipeline("sentiment-analysis")
    sentiment = sentiment_pipeline(json['review'])

    return { "sentiment": sentiment[0]['label'].lower() }
 
# main driver function
if __name__ == '__main__':
    app.run(port=9000)