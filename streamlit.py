import pickle
import tensorflow as tf
import numpy as np
import streamlit as st

podcast_review_model = pickle.load(open('podcast_review_model.pkl', 'rb'))
tokenizer = pickle.load(open('tokenizer.pkl', 'rb'))

SEQ_LEN = 128
def prepare_texts(texts: list) -> np.array:
    return tf.keras.preprocessing.sequence.pad_sequences(tokenizer.texts_to_sequences(texts), maxlen=SEQ_LEN)

title = st.text_input('Podcast Title:')

review = st.text_input('Podcast review:')
st.write('Your review includes: ', review)

if review:
    prediction = podcast_review_model.predict(prepare_texts([review])).argmax(axis=1)

    if prediction.size != 0: 
        if (prediction[0] == 0):
            sentiment = "Negative :("
        else:
            sentiment = "Positive :)"
        st.write('Probable sentiment:', sentiment)
