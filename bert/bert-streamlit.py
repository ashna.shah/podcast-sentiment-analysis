from transformers import pipeline
import streamlit as st

sentiment_pipeline = pipeline("sentiment-analysis")

title = st.text_input('Podcast Title:')
review = st.text_input('Podcast Review:')
st.write('Your review includes: ', review)

if review:
    sentiment = sentiment_pipeline(review)
    st.write('Probable sentiment:', sentiment[0]['label'])
